<?php
/**
 * @file
 * Admin Forms.
 *
 * @author Ying Ho <ying@openconcept.dk>
 */

/**
 * Admin Form for choosing image styles.
 */
function imce_image_style_admin_form($form, &$form_state) {
  $presets = array();
  $styles = image_styles();
  foreach ($styles as $name => $style) {
    $presets[$name] = $style['label'];
  }

  $form['imce_image_style_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IMCE Image style'),
    '#default_value' => variable_get('imce_image_style_enable', FALSE),
  );

  $form['imce_image_style_preset'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Image styles to enable in IMCE'),
    '#options' => $presets,
    '#multiple' => TRUE,
    '#description' => t('Select image styles names that the user can choose when uploading image file via IMCE.'),
    '#default_value' => variable_get('imce_image_style_preset', array()),
  );

  $node_types = node_type_get_types();
  foreach ($node_types as $node_type => $object) {
    $form['imce_image_style_' . $node_type] = array(
      '#type' => 'fieldset',
      '#title' => t('!node_type with default image styles', array('!node_type' => $node_type)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['imce_image_style_' . $node_type]['imce_image_style_preset_' . $node_type] = array(
      '#type' => 'checkboxes',
      '#title' => t('Image styles to default in IMCE'),
      '#options' => $presets,
      '#multiple' => TRUE,
      '#description' => t('Select image styles names that this node type with default image styles when uploading image file via IMCE.'),
      '#default_value' => variable_get('imce_image_style_preset_' . $node_type, array()),
    );
  }

  return system_settings_form($form);
}
