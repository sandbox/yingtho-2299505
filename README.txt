
IMCE Mkdir
http://drupal.org/project/imce_image_style
====================================


DESCRIPTION
-----------
Adds image styles to files uploaded via IMCE.

Note: For content type preset with images styles you need to add a patch to ckeditor, https://www.drupal.org/files/issues/ckeditor-imce-current-path-2300767-1.patch.


INSTALLING
----------
1. To install the module copy the 'imce_image_style' folder to your sites/all/modules directory.

2. Go to module administration page. Enable the module.
Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
1. Go to /admin/config/media/imce_image_style.

2. Enable the IMCE Image style and choose which image style the user can choose to use when upload files via IMCE.

3. Click on 'Save configuration' button.

4. To test use IMCE. When uploading new files in IMCE then you can see the image style, you have chosen above, that can be applied to the uploaded file.


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE. CONTRIBUTE.
------------------------------------------------------------------------
1. Go to the module issue queue at http://drupal.org/project/issues/imce_image_style?status=All&categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to http://drupal.org/project/issues/user
